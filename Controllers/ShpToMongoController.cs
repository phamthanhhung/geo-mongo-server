﻿using GeoMongoServer.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using MongoDB.Driver.GeoJsonObjectModel;
using OSGeo.GDAL;
using OSGeo.OGR;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace GeoMongoServer.Controllers
{
    [ApiController]
    [Route("api/")]
    public class ShpToMongoController : ControllerBase
    {
        protected static IMongoClient _client;
        protected static IMongoDatabase _database;
        protected static string _folderPath;
        protected static string _unzipFolderName;

        public ShpToMongoController()
        {
            _client = new MongoClient("mongodb://localhost:27017");
            _database = _client.GetDatabase("geodb");
            _folderPath = "C:\\Users\\Admin\\Downloads\\vungkt\\";
            _unzipFolderName = "unzip";
        }

        [HttpGet("get-data")]
        public IActionResult Get([FromQuery] string layername, string bbox, int zoom)
        {
            if (zoom < 15)
            {
                return NoContent();
            }
            var bboxArray = bbox.Split(',');
            double x1 = Double.Parse(bboxArray[0]);
            double y1 = Double.Parse(bboxArray[1]);
            double x2 = Double.Parse(bboxArray[2]);
            double y2 = Double.Parse(bboxArray[3]);

            var _collection = _database.GetCollection<ShpGeojsonModel>(layername);

            var point = GeoJson.Polygon(GeoJson.Geographic(x1, y1), GeoJson.Geographic(x1, y2), GeoJson.Geographic(x2, y2), GeoJson.Geographic(x2, y1), GeoJson.Geographic(x1, y1));

            var locationQuery = new FilterDefinitionBuilder<ShpGeojsonModel>().GeoIntersects("geometry", point);

            var nearData = _collection.Find(locationQuery);

            var data = nearData.ToList();

            return Ok(data);
        }

        [HttpPost("upload-shp")]
        public async Task<IActionResult> UploadShp(IFormFile file)
        {
            if (file == null || file.Length == 0)
                return Content("file not selected");

            var path = _folderPath + file.FileName;

            using (var stream = new FileStream(path, FileMode.Create))
            {
                await file.CopyToAsync(stream);
            }

            if (Directory.Exists(_folderPath + _unzipFolderName))
            {
                Directory.Delete(_folderPath + _unzipFolderName, true);
            }
            // tạo folder giải nén
            System.IO.Directory.CreateDirectory(_folderPath + _unzipFolderName);
            // giải nén
            System.IO.Compression.ZipFile.ExtractToDirectory(path, _folderPath + _unzipFolderName);

            // lấy info file shp
            DirectoryInfo d = new DirectoryInfo(_folderPath + _unzipFolderName);
            FileInfo shpFile = d.GetFiles("*.shp")[0];

            var fileName = Path.GetFileNameWithoutExtension(shpFile.FullName);

            var _collection = _database.GetCollection<BsonDocument>(fileName);

            // create geo index
            var index = Builders<BsonDocument>.IndexKeys.Geo2DSphere("geometry");
            _collection.Indexes.CreateOne(new CreateIndexModel<BsonDocument>(index));


            // a common process is : datasource (ds) -> layer -> feature -> geometry

            Gdal.AllRegister();
            Ogr.RegisterAll();

            string shapeFilePath = shpFile.FullName;

            var drv = Ogr.GetDriverByName("ESRI Shapefile");

            var ds = drv.Open(shapeFilePath, 0);

            OSGeo.OGR.Layer layer = ds.GetLayerByIndex(0);
            OSGeo.OGR.Feature f;
            layer.ResetReading();

            System.Text.StringBuilder sb = new System.Text.StringBuilder();

            var listDocument = new List<BsonDocument>();
            //sb.AppendLine("{\"type\":\"FeatureCollection\", \"features\":[");

            while ((f = layer.GetNextFeature()) != null)
            {
                sb.Clear();
                //Geometry
                var geom = f.GetGeometryRef();
                if (geom != null)
                {
                    var geometryJson = geom.ExportToJson(new string[] { });
                    sb.Append("{\"type\":\"Feature\",\"geometry\":" + geometryJson + ",");
                }

                //Properties
                int count = f.GetFieldCount();
                if (count != 0)
                {
                    sb.Append("\"properties\":{");
                    for (int i = 0; i < count; i++)
                    {
                        FieldType type = f.GetFieldType(i);
                        string key = f.GetFieldDefnRef(i).GetName();

                        if (type == FieldType.OFTInteger)
                        {
                            var field = f.GetFieldAsInteger(i);
                            sb.Append("\"" + key + "\":" + field + ",");
                        }
                        else if (type == FieldType.OFTReal)
                        {
                            var field = f.GetFieldAsDouble(i);
                            sb.Append("\"" + key + "\":" + field + ",");
                        }
                        else
                        {
                            var field = f.GetFieldAsString(i);
                            sb.Append("\"" + key + "\":\"" + field + "\",");
                        }

                    }
                    sb.Length--;
                    sb.Append("},");
                }

                //FID
                long id = f.GetFID();
                sb.AppendLine("\"id\":" + id + "}\n");
                var document = BsonSerializer.Deserialize<BsonDocument>(sb.ToString());
                listDocument.Add(document);

            }
            _collection.InsertMany(listDocument);
            return Ok();
        }

        [HttpGet("getFeatureInfo")]
        public IActionResult getFeatureInfo([FromQuery] string layername, double lon, double lat)
        {

            var _collection = _database.GetCollection<ShpGeojsonModel>(layername);

            var point = GeoJson.Point(GeoJson.Geographic(lon, lat));

            var locationQuery = new FilterDefinitionBuilder<ShpGeojsonModel>().GeoIntersects("geometry", point);

            var nearData = _collection.Find(locationQuery);

            var data = nearData.ToList();

            return Ok(data);
        }
    }
}
